{ zentralwerk }:

{ config, lib, ... }:

{
  options.deployment = with lib; {
    vcpu = mkOption {
      default = 4;
      type = types.int;
    };
    mem = mkOption {
      default = 512;
      type = types.int;
    };
    hypervisor = mkOption {
      default = "cloud-hypervisor";
      type = types.enum [
        "qemu"
        "cloud-hypervisor"
        "firecracker"
        "crosvm"
        "kvmtool"
      ];
    };
    networks = mkOption {
      type = with types; listOf str;
    };
    rootSize = mkOption {
      # 2 GB
      default = 2048;
      type = types.int;
    };
    persistedShares = mkOption {
      default = [];
      type = with types; listOf str;
    };
    extraShares = mkOption {
      default = [];
      type = with types; listOf (submodule {
        options = {
          source = mkOption {
            type = str;
          };
          mountPoint = mkOption {
            type = str;
          };
        };
      });
      description = ''
        Extra shares. THESE MUST BE AVAILABLE ON ALL MICROVM HOSTS!
      '';
    };
    needForSpeed = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Prefer deployment on Nomad clients with a higher c3d2.cpuSpeed
      '';
    };
  };

  config = {
    deployment.networks = builtins.attrNames (
      lib.filterAttrs (_: { hosts4, hosts6, ... }:
        hosts4 ? ${config.networking.hostName} ||
        lib.filterAttrs (_: hosts6:
          hosts6 ? ${config.networking.hostName}
        ) hosts6 != {}
      ) zentralwerk.lib.config.site.net
    );
  };
}
