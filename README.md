# Deployment options

Extracted `options.deployment` from [c3d2/nix-config](https://gitea.c3d2.de/c3d2/nix-config) for easier reuse without adding 50 flake inputs.
