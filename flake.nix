{
  description = "C3D2 cluster deployment options";

  inputs = {
    zentralwerk = {
      url = "git+https://gitea.c3d2.de/zentralwerk/network.git";
    };
  };
  
  outputs = { self, zentralwerk, ... }: {
    nixosModules.deployment-options = import ./options.nix {
      inherit zentralwerk;
    };
  };
}
